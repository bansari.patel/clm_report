<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function(){
Route::get('/welcome', 'HomeController@index')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('asset/type/list','AssetTypeController@index')->name('asset_type_list');
Route::get('asset/type/add','AssetTypeController@add')->name('asset_type_add');
Route::post('asset/type/add','AssetTypeController@save_add')->name('asset_type_save');
Route::get('asset/type/edit/{id}','AssetTypeController@edit')->name('asset_type_edit_view');
Route::post('asset/type/edit','AssetTypeController@save_edit')->name('asset_type_edit');
Route::get('asset/type/delete/{id}','AssetTypeController@delete')->name('asset_type_delete');

Route::get('site/list','SiteController@index')->name('site_list');
Route::get('site/add','SiteController@add')->name('site_add');
Route::post('site/add','SiteController@save_add')->name('site_save');
Route::get('site/edit/{id}','SiteController@edit')->name('site_edit_view');
Route::post('site/edit','SiteController@save_edit')->name('site_edit');
Route::get('site/delete/{id}','SiteController@delete')->name('site_delete');

Route::get('asset/list','AssetController@index')->name('asset_list');
Route::get('asset/add','AssetController@add')->name('asset_add');
Route::post('asset/add','AssetController@save_add')->name('asset_save');
Route::get('asset/edit/{id}','AssetController@edit')->name('asset_edit_view');
Route::post('asset/edit','AssetController@save_edit')->name('asset_edit');
Route::get('asset/delete/{id}','AssetController@delete')->name('asset_delete');


Route::get('inspection/list','InspectionController@index')->name('inspection_list');
Route::get('inspection/add','InspectionController@add')->name('inspection_add');
Route::post('inspection/add','InspectionController@save_add')->name('inspection_save');
Route::get('inspection/edit/{id}','InspectionController@edit')->name('inspection_edit_view');
Route::post('inspection/edit','InspectionController@save_edit')->name('inspection_edit');
Route::get('inspection/delete/{id}','InspectionController@delete')->name('inspection_delete');


Route::get('asset/inspection/list','AssetInspectionController@index')->name('asset_inspection_list');
Route::get('asset/inspection/add','AssetInspectionController@add')->name('asset_inspection_add');
Route::post('asset/inspection/add','AssetInspectionController@save_add')->name('asset_inspection_save');
Route::get('asset/inspection/edit/{id}','AssetInspectionController@edit')->name('asset_inspection_edit_view');
Route::post('asset/inspection/edit','AssetInspectionController@save_edit')->name('asset_inspection_edit');
Route::get('asset/inspection/delete/{id}','AssetInspectionController@delete')->name('asset_inspection_delete');

Route::get('asset/inspection/report/list','AssetInspectionReportController@index')->name('asset_inspection_report_list');
Route::get('asset/inspection/report/add','AssetInspectionReportController@add')->name('asset_inspection_report_add');
Route::post('asset/inspection/report/add','AssetInspectionReportController@save_add')->name('asset_inspection_report_save');
Route::get('get/asset/type/{id}','AssetInspectionReportController@asset_type_list')->name('get_asset_type_list');
Route::get('get/inspection/{id}/{aId}','AssetInspectionReportController@inspection_list')->name('get_inspection_list');

Route::get('report/view','ReportController@index')->name('report_view');
Route::get('report/list','ReportController@list')->name('report_list');
Route::post('report/list','ReportController@filter_list')->name('filter_list');

});


@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formValidate").validate({
            rules: {
                name : {
                    required: true
                }
            },
            messages : {
                name: {
                    required: "Please enter name"
                }
            }
        });
    });
</script>
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('asset_edit')}}">
                        {{csrf_field() }}
                            <h5 class="form-header">
                                Edit Asset
                                <a class="btn btn-sm btn-secondary" href="{{route('asset_list')}}">Back</a>
                            </h5>
                            <div class="form-group">
                                    <label for="">Asset Type</label>
                                    <select class="form-control" required="required"  name="asset_type_id" id="asset_type_id">
                                        <option value="">Select</option>
                                        @foreach($asset_types as $asset_type)
                                            <option value="{{$asset_type->id}}" @if($edit->asset_type_id == $asset_type->id) selected @endif>{{$asset_type->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('asset_type_id')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('asset_type_id') }}</strong>
                                    </span>
                                    @enderror
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                                
                                <div class="form-group">
                                        <label for="">Site</label>
                                        <select class="form-control" required="required"  name="site_id" id="site_id">
                                            <option value="">Select</option>
                                            @foreach($sites as $site)
                                                <option value="{{$site->id}}" @if($edit->site_id == $site->id) selected @endif>{{$site->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('site_id')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('site_id') }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="hidden" name="id" value="{{$edit->id}}">
                                <input class="form-control" data-error="Your name is required" placeholder="Enter Cateory Name" required="required" type="text" name="name" value = "{{$edit->name}}">
                                @error('name')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>

                            <div class="form-group">
                                <label for="">Status</label>
                                <select class="form-control" required="required"  name="status" id="status">
                                    <option value="">Select</option>
                                    <option value="1" @if($edit->status == 1) selected @endif>Active</option>
                                    <option value="0" @if($edit->status == 0) selected @endif>InActive</option>
                                </select>
                                @error('status')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.admin')
@section('content')
<div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <div class="element-box">
          <h5 class="form-header">
            Asset
            <a class="btn btn-primary float-right" href="{{route('asset_add')}}">Add</a>
          </h5>
          <div class="table-responsive">
            <table id="dataTable1" class="table table-striped table-lightfont">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Asset Type</th>
                  <th>Site</th>
                  <th>Status</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($assets as $asset) { ?>
                <tr>
                  <td>{{$asset->name}}</td>
                  <td>{{$asset->asset_type->name}}</td>
                  <td>{{$asset->site->name}}</td>
                  <td>@if($asset->status == 1) <span class="status-pill green"></span><span>Active</span> @else <span class="status-pill red"></span><span>InActive</span> @endif</td>
                  <td>
                    <a href="{{route('asset_edit_view',[$asset->id])}}" title="Edit"><i class="icon-feather-edit-2"></i></a> &nbsp;&nbsp;
					          <a href="{{route('asset_delete',[$asset->id])}}" onclick="return confirm('Are you sure you want to delete this Asset?');"  title="Delete"><i class="icon-feather-trash-2"></i></a> &nbsp;&nbsp;
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@extends('layouts.admin')
@section('content')
<div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <div class="element-box">
          <h5 class="form-header">
           Accept Inspection
            <a class="btn btn-primary float-right" href="{{route('asset_inspection_add')}}">Add</a>
          </h5>
          <div class="table-responsive">
            <table id="dataTable1" class="table table-striped table-lightfont">
              <thead>
                <tr>
                  <th>Asset</th>
                  <th>Inspection</th>
                  <th>Last Due date</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($assetInspections as $insp) { ?>
                <tr>
                  <td>{{$insp->asset->name}}</td>
                  <td>{{$insp->inspection->name}}</td>
                  <td>{{$insp->last_due_date}}</td>
                  <td>
                    <a href="{{route('asset_inspection_edit_view',[$insp->id])}}" title="Edit"><i class="icon-feather-edit-2"></i></a> &nbsp;&nbsp;
					          <a href="{{route('asset_inspection_delete',[$insp->id])}}" onclick="return confirm('Are you sure you want to delete this Asset Inspection?');"  title="Delete"><i class="icon-feather-trash-2"></i></a> &nbsp;&nbsp;
					
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $(document).ready(function() {
            $("#formValidate").validate({
                rules: {
                    asset_id : {
                        required: true
                    },
                    inspection_id:{
                        required:true
                    },
                    last_due_date:{
                        required:true
                    }
                },
                messages : {
                    last_due_date: {
                        required: "Please enter last due date"
                    },
                    inspection_id:{
                        required: "Please enter inspection id"   
                    },
                    asset_id:{
                        required: "Please enter asset id"   
                    }
    
                }
            });
        });
</script>
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('asset_inspection_edit')}}">
                        {{csrf_field() }}
                            <h5 class="form-header">
                                Edit Asset Inspection
                                <a class="btn btn-sm btn-secondary" href="{{route('asset_inspection_list')}}">Back</a>
                            </h5>
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$edit->id}}">
                                <label for="">Asset</label>
                                <select class="form-control" required="required" name="asset_id" id="asset_id">
                                    <option value=""> Select</option>
                                    @foreach($assets as $asset)
                                        <option value="{{$asset->id}}" @if($asset->id == $edit->asset_id) selected @endif> {{$asset->name}}</option>
                                    @endforeach
                                </select>
                                @error('asset_id')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('asset_id') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Inspection</label>
                                <select class="form-control" required="required" name="inspection_id" id="inspection_id">
                                    <option value="">Select</option>
                                    @foreach($inspections as $inspection)
                                        <option value="{{$inspection->id}}" @if($inspection->id == $edit->inspection_id) selected @endif>{{$inspection->name}}</option>
                                    @endforeach
                                </select>

                                @error('inspection_id')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('inspection_id') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Last Due Date</label>
                                <input class="form-control inputBox" placeholder="Enter Last Due Date" required="required" type="date" name="last_due_date" id="example-date-input last_due_date" value="{{$edit->last_due_date}}">
                                @error('last_due_date')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('last_due_date') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!DOCTYPE html>
<html>
  <head>
    <title>Admin Dashboard HTML Template</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="<?php echo URL('/'); ?>/admin/favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/css/main.css?version=4.5.0" rel="stylesheet">
  </head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formValidate").validate({
            rules: {
                email : {
                    required: true
                },
                password:{
                    required: true
                }
            },
            messages : {
                email: {
                    required: "Please enter email"
                },
                password:{
                    required: "please enter password"
                }
            }
        });
    });
</script>
  <body class="auth-wrapper">
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w">
        <div class="logo-w">
          <a href="index.html"><img alt="" src="/admin/img/logo-big.png"></a>
        </div>
        <h4 class="auth-header">
          Login Form
        </h4>
        <form method="POST" action="{{ route('login') }}">
                @csrf

          <div class="form-group">
            <label for="">Email</label><input class="form-control" placeholder="Enter your username" type="text" name="email" id="email">
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="">Password</label><input class="form-control" placeholder="Enter your password" type="password" name="password" id="password">
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="buttons-w">
            <button class="btn btn-primary">Log me in</button>
            
          </div>
        </form>
      </div>
    </div>
  </body>
</html>

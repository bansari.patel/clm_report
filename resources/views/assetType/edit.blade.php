@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formValidate").validate({
            rules: {
                name : {
                    required: true
                }
            },
            messages : {
                name: {
                    required: "Please enter name"
                }
            }
        });
    });
</script>
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('asset_type_edit')}}">
                        {{csrf_field() }}
                            <h5 class="form-header">
                                Edit Asset Type
                                <a class="btn btn-sm btn-secondary" href="{{route('asset_type_list')}}">Back</a>
                            </h5>
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="hidden" name="id" value="{{$edit->id}}">
                                <input class="form-control" data-error="Your name is required" placeholder="Enter Cateory Name" required="required" type="text" name="name" value = "{{$edit->name}}">
                                @error('name')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select class="form-control" required="required"  name="status" id="status">
                                    <option value="">Select</option>
                                    <option value="1" @if($edit->status == 1) selected @endif>Active</option>
                                    <option value="0" @if($edit->status == 0) selected @endif>InActive</option>
                                </select>
                                @error('site_id')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('site_id') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.admin')
@section('content')
<div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <div class="element-box">
          <h5 class="form-header">
           Accept Inspection Report
            <a class="btn btn-primary float-right" href="{{route('asset_inspection_report_add')}}">Add</a>
          </h5>
          <div class="table-responsive">
            <table id="dataTable1" class="table table-striped table-lightfont">
              <thead>
                <tr>
                  <th>Asset</th>
                  <th>Inspection</th>
                  <th>Next Due Date</th>
                  <th>Last Due Date</th>
                  
                </tr>
              </thead>
              <tbody>
              <?php foreach ($lists as $insp) { ?>
                <tr>
                  <td>{{$insp->asset->name}}</td>
                  <td>{{$insp->inspection->name}}</td>
                  <td>{{$insp->next_due_date}}</td>
                  <td>{{$insp->last_due_date}}</td>
                  
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

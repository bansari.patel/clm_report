@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formValidate").validate({
            rules: {
                asset_id : {
                    required: true
                },
                inspection_id:{
                    required:true
                },
                date_inspected:{
                    required:true
                },
                due_date:{
                    required:true
                }
            },
            messages : {
                due_date: {
                    required: "Please enter due date"
                },
                date_inspected: {
                    required: "Please enter date inspected"
                },
                inspection_id:{
                    required: "Please enter inspection id"   
                },
                asset_id:{
                    required: "Please enter asset id"   
                }

            }
        });
    });
</script>
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('asset_inspection_report_save')}}">
                        {{csrf_field() }}
                            <h5 class="form-header">
                                Add Asset Inspection Report
                                <a class="btn btn-sm btn-secondary" href="{{route('asset_inspection_list')}}">Back</a>
                            </h5>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Asset Number</label>
                                        <select class="form-control" required="required" name="asset_id" id="asset_id">
                                            <option value=""> Select</option>
                                            @foreach($assets as $asset)
                                                <option value="{{$asset->id}}"> {{$asset->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('asset_id')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('asset_id') }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                    </div>
                            
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Component / Inspection</label>
                                        <select class="form-control" required="required" name="inspection_id" id="inspection_id">
                                            <option value="">Select</option>
                                            @foreach($inspections as $inspection)
                                                <option value="{{$inspection->id}}">{{$inspection->name}}</option>
                                            @endforeach
                                        </select>

                                        @error('inspection_id')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('inspection_id') }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>
                            
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Date Inspected</label>
                                        <input class="form-control inputBox" placeholder="Enter Date Inspected" required="required" type="text" name="date_inspected" id="date_inspected" readonly>
                                        @error('date_inspected')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('date_inspected') }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Due Date</label>
                                        <input class="form-control inputBox" placeholder="Enter Due Date" required="required" type="text" name="due_date" id="due_date" readonly>
                                        @error('due_date')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('due_date') }}</strong>
                                        </span>
                                        @enderror
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-buttons buttonBox">
                                            <button class="btn btn-primary buttonText" type="submit"> Enter Data</button>
                                    </div>
                                </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Asset Type</label>
                                    <select class="form-control" required="required" name="asset_type" id="asset_type" readonly>
                                        
                                    </select>

                                    @error('asset_type')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('asset_type') }}</strong>
                                    </span>
                                    @enderror
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
                        
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">LifeCycle Frequency</label>
                                    <input class="form-control inputBox" placeholder="Enter LifeCycle Frequency" required="required" type="text" name="life_cycle_frequency" id="life_cycle_frequency" readonly>
                                    
                                    @error('life_cycle_frequency')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('life_cycle_frequency') }}</strong>
                                    </span>
                                    @enderror
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                            </div>
    
                        </div>                          
                            
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="asset_id"]').on('change', function() {
            var id = $(this).val();
            if(id) {
                $.ajax({
                    url: '/get/asset/type/'+id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="asset_type"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="asset_type"]').append('<option value="'+ value['id'] +'">'+ value['name'] +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="asset_type"]').empty();
            }
        });

        $('select[name="inspection_id"]').on('change', function() {
            var id = $(this).val();
            var assetId = document.getElementById("asset_id").value;
            if(id && assetId) {
                $.ajax({
                    url: '/get/inspection/'+id+'/' +assetId,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        alert(data);
                        if(data['inspection']['frequency'] == 1) {
                            var frequency = 'Month'
                            var month = data['inspection']['value'];
                        }else{
                            var frequency = 'Year' 
                            var year = data['inspection']['value'];
                            var month = year * 12 ;
                        }

                        
                            var date_inspected = new Date(data['last_due_date']);
                            var datess = date_inspected.setMonth(date_inspected.getMonth() + month);
                            var due_date = moment(datess).format('YYYY-MM-DD');
                            
                            document.getElementById("life_cycle_frequency").value = data['inspection']['value'] +" "+frequency;
                            document.getElementById("date_inspected").value = data['last_due_date'];
                            document.getElementById("due_date").value = due_date;
                        
                    }
                });
            }else{
                alert('Please select Asset and Inspection');
                
            }
        });
    });
</script>
@endsection
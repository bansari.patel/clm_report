@extends('layouts.admin')
@section('content')
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                            <h5 class="form-header">
                                Report
                                
                            </h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"></div>
                            
                            <div class="col-md-5">
                            <div class="form-buttons listButtonBox">
                                <a href="{{route('report_list')}}" class="btn btn-primary buttonText" type="submit"> Go to Report</a>
                                
                            </div>
                            </div>
                        </div>
                            <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('filter_list')}}">
                                {{csrf_field() }}
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Asset Type  Report</label>
                                            <select class="form-control"  name="asset_type_id" id="asset_type_id">
                                                <option value=""> Select</option>
                                                @foreach($asset_types as $type)
                                                    <option value="{{$type->id}}"> {{$type->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('asset_type_id')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('asset_type_id') }}</strong>
                                            </span>
                                            @enderror
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                    <div class="form-buttons listButtonBox">
                                        <button class="btn btn-primary buttonText" type="submit" name="asset_type_btn" value="1"> Go to Asset Type Report</button>
                                    </div>
                                </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Asset Report</label>
                                            <select class="form-control"  name="asset_id" id="asset_id">
                                                <option value=""> Select</option>
                                                @foreach($assets as $asset)
                                                    <option value="{{$asset->id}}"> {{$asset->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('asset_id')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('asset_id') }}</strong>
                                            </span>
                                            @enderror
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-buttons listButtonBox">
                                            <button class="btn btn-primary buttonText" type="submit" name="asset_btn" value="1"> Go to Asset Report</button>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="">Site Report</label>
                                            <select class="form-control"  name="site_id" id="site_id">
                                                <option value=""> Select</option>
                                                @foreach($sites as $site)
                                                    <option value="{{$site->id}}"> {{$site->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('site_id')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('site_id') }}</strong>
                                            </span>
                                            @enderror
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                            <div class="form-buttons listButtonBox">
                                                <button class="btn btn-primary buttonText" type="submit" name="site_btn" value="1"> Go to Site Report</button>
                                            </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="">Forecast Report</label>
                                            <select class="form-control"  name="date_id" id="date_id">
                                                <option value=""> Select</option>
                                                <option value="0"> This Month</option>
                                                <option value="1"> Next Month</option>
                                                <option value="2"> Next 2 Month</option>
                                                <option value="3"> Next 3 Month</option>
                                                <option value="4"> Next 4 Month</option>
                                                <option value="5"> Next 5 Month</option>
                                                <option value="6"> Next 6 Month</option>
                                                <option value="7"> Next 7 Month</option>
                                                <option value="8"> Next 8 Month</option>
                                                <option value="9"> Next 9 Month</option>
                                                <option value="10"> Next 10 Month</option>
                                                <option value="11"> Next 11 Month</option>
                                                <option value="12"> Next 12 Month</option>
                                                
                                                
                                            </select>
                                            @error('date_id')
                                            <span class="help-block" role="alert">
                                                <strong>{{ $errors->first('date_id') }}</strong>
                                            </span>
                                            @enderror
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                            <div class="form-buttons listButtonBox">
                                                <button class="btn btn-primary buttonText" type="submit" name="forecast_btn" value="1"> Go to Forecast Report</button>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
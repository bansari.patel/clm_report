@extends('layouts.admin')
@section('content')
<div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <div class="element-box">
          <h5 class="form-header">
            Inspection
            <a class="btn btn-primary float-right" href="{{route('inspection_add')}}">Add</a>
          </h5>
          <div class="table-responsive">
            <table id="dataTable1" class="table table-striped table-lightfont">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Frequency</th>
                  <th>Value</th>
                  <th>Rate</th>
                  <th>Status</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($inspections as $inspection) { ?>
                <tr>
                  <td>{{$inspection->name}}</td>
                  <td>@if($inspection->frequency == 1) Monthly @else Yearly @endif</td>
                  <td>{{$inspection->value}}</td>
                  <td>{{$inspection->rate}}</td>
                  <td>@if($inspection->status == 1) <span class="status-pill green"></span><span>Active</span> @else <span class="status-pill red"></span><span>InActive</span> @endif</td>
                  <td>
                    <a href="{{route('inspection_edit_view',[$inspection->id])}}" title="Edit"><i class="icon-feather-edit-2"></i></a> &nbsp;&nbsp;
					          <a href="{{route('inspection_delete',[$inspection->id])}}" onclick="return confirm('Are you sure you want to delete this Asset Type?');"  title="Delete"><i class="icon-feather-trash-2"></i></a> &nbsp;&nbsp;
					
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

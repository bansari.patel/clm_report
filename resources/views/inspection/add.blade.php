@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formValidate").validate({
            rules: {
                name : {
                    required: true
                },
                frequency:{
                    required:true
                },
                value:{
                    required:true
                },rate:{
                    required:true
                }
            },
            messages : {
                name: {
                    required: "Please enter name"
                },
                frequency:{
                    required: "Please enter frequency"   
                },
                value:{
                    required: "Please enter value"   
                },
                rate:{
                    required: "Please enter rate"   
                }

            }
        });
    });
</script>
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('inspection_save')}}">
                        {{csrf_field() }}
                            <h5 class="form-header">
                                Add Inspection
                                <a class="btn btn-sm btn-secondary" href="{{route('inspection_list')}}">Back</a>
                            </h5>
                            <div class="form-group">
                                <label for="">Name</label>
                                <input class="form-control" placeholder="Enter Name" required="required" type="text" name="name">
                                @error('name')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Frequency</label>
                                <select class="form-control" placeholder="Enter frequency" required="required" name="frequency">
                                    <option value="">Select</option>
                                    <option value="1">Monthly</option>
                                    <option value="2">Yearly</option>
                                </select>

                                @error('frequency')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('frequency') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Value</label>
                                <input class="form-control" placeholder="Enter value" required="required" type="text" name="value">
                                @error('value')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('value') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Rate</label>
                                <input class="form-control" placeholder="Enter Rate" required="required" type="text" name="rate">
                                @error('rate')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('rate') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
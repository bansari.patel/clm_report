@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#formValidate").validate({
            rules: {
                name : {
                    required: true
                }
            },
            messages : {
                name: {
                    required: "Please enter name"
                }
            }
        });
    });
</script>
<div class="content-i">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" enctype="multipart/form-data" method="post" action="{{route('site_save')}}">
                        {{csrf_field() }}
                            <h5 class="form-header">
                                Add Site
                                <a class="btn btn-sm btn-secondary" href="{{route('site_list')}}">Back</a>
                            </h5>
                            <div class="form-group">
                                <label for="">Name</label>
                                <input class="form-control" placeholder="Enter Name" required="required" type="text" name="name">
                                @error('name')
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                                <div class="help-block form-text with-errors form-control-feedback"></div>
                            </div>
                            
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
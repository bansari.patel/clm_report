@extends('layouts.admin')
@section('content')
<div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <div class="element-box">
          <h5 class="form-header">
            Site 
            <a class="btn btn-primary float-right" href="{{route('site_add')}}">Add</a>
          </h5>
          <div class="table-responsive">
            <table id="dataTable1" class="table table-striped table-lightfont">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Status</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($sites as $site) { ?>
                <tr>
                  <td>{{$site->name}}</td>
                  <td>@if($site->status == 1) <span class="status-pill green"></span><span>Active</span> @else <span class="status-pill red"></span><span>InActive</span> @endif</td>
                  <td>
                    <a href="{{route('site_edit_view',[$site->id])}}" title="Edit"><i class="icon-feather-edit-2"></i></a> &nbsp;&nbsp;
					          <a href="{{route('site_delete',[$site->id])}}" onclick="return confirm('Are you sure you want to delete this Site?');"  title="Delete"><i class="icon-feather-trash-2"></i></a> &nbsp;&nbsp;
					
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

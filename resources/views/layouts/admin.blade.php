<!DOCTYPE html>
<html>
  <head>
    <title>CLM Report | Admin</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="shortcut icon" href="<?php echo URL('/'); ?>/theme/images/favicon.jpg">
    <link rel="apple-touch-icon" href="<?php echo URL('/'); ?>/theme/images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo URL('/'); ?>/theme/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo URL('/'); ?>/theme/images/apple-touch-icon-114x114.png">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/css/main.css?version=4.5.0" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/css/custom.css?version=4.5.0" rel="stylesheet">
    <link href="<?php echo URL('/'); ?>/admin/icon_fonts_assets/feather/style.css" rel="stylesheet">

  </head>
  <body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">
        <!-- <div aria-hidden="true" class="onboarding-modal modal fade animated show-on-load" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-centered" role="document">
                <div class="modal-content text-center">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span class="close-label">Skip Intro</span><span class="os-icon os-icon-close"></span></button>
                    <div class="onboarding-slider-w">
                        <div class="onboarding-slide">
                            <div class="onboarding-media">
                                <img alt="" src="<?php echo URL('/'); ?>/admin/img/bigicon2.png" width="200px">
                            </div>
                            <div class="onboarding-content with-gradient">
                                <h4 class="onboarding-title">
                                    Example of onboarding screen!
                                </h4>
                                <div class="onboarding-text">
                                    This is an example of a multistep onboarding screen, you can use it to introduce your customers to your app, or collect additional information from them before they start using your app.
                                </div>
                            </div>
                        </div>
                        <div class="onboarding-slide">
                            <div class="onboarding-media">
                                <img alt="" src="<?php echo URL('/'); ?>/admin/img/bigicon5.png" width="200px">
                            </div>
                            <div class="onboarding-content with-gradient">
                                <h4 class="onboarding-title">
                                    Example Request Information
                                </h4>
                                <div class="onboarding-text">
                                    In this example you can see a form where you can request some additional information from the customer when they land on the app page.
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Your Full Name</label><input class="form-control" placeholder="Enter your full name..." type="text" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Your Role</label>
                                                <select class="form-control">
                                                    <option>
                                                        Web Developer
                                                    </option>
                                                    <option>
                                                        Business Owner
                                                    </option>
                                                    <option>
                                                        Other
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="onboarding-slide">
                            <div class="onboarding-media">
                                <img alt="" src="<?php echo URL('/'); ?>/admin/img/bigicon6.png" width="200px">
                            </div>
                            <div class="onboarding-content with-gradient">
                                <h4 class="onboarding-title">
                                    Showcase App Features
                                </h4>
                                <div class="onboarding-text">
                                    In this example you can showcase some of the features of your application, it is very handy to make new users aware of your hidden features. You can use boostrap columns to split them up.
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <ul class="features-list">
                                            <li>
                                                Fully Responsive design
                                            </li>
                                            <li>
                                                Pre-built app layouts
                                            </li>
                                            <li>
                                                Incredible Flexibility
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="features-list">
                                            <li>
                                                Boxed & Full Layouts
                                            </li>
                                            <li>
                                                Based on Bootstrap 4
                                            </li>
                                            <li>
                                                Developer Friendly
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="search-with-suggestions-w">
            <div class="search-with-suggestions-modal">
                {{-- <div class="element-search">
                    <input class="search-suggest-input" placeholder="Start typing to search..." type="text">
                        <div class="close-search-suggestions">
                            <i class="os-icon os-icon-x"></i>
                        </div>
                    </input>
                </div> --}}
                <div class="search-suggestions-group">
                    <div class="ssg-header">
                        <div class="ssg-icon">
                            <div class="os-icon os-icon-box"></div>
                        </div>
                        <div class="ssg-name">
                            Projects
                        </div>
                        <div class="ssg-info">
                            24 Total
                        </div>
                    </div>
                    <div class="ssg-content">
                        <div class="ssg-items ssg-items-boxed">
                            <a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(<?php echo URL('/'); ?>/admin/img/company6.png)"></div>
                                <div class="item-name">
                                    Integ<span>ration</span> with API
                                </div>
                            </a>
                            <a class="ssg-item" href="users_profile_big.html">
                                <div class="item-media" style="background-image: url(<?php echo URL('/'); ?>/admin/img/company7.png)"></div>
                                <div class="item-name">
                                    Deve<span>lopm</span>ent Project
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="search-suggestions-group">
                    <div class="ssg-header">
                        <div class="ssg-icon">
                            <div class="os-icon os-icon-users"></div>
                        </div>
                        <div class="ssg-name">
                            Customers
                        </div>
                        <div class="ssg-info">
                            12 Total
                        </div>
                    </div>
                    <div class="ssg-content">
                        <div class="ssg-items ssg-items-list">
                        <a class="ssg-item" href="users_profile_big.html">
                            <div class="item-media" style="background-image: url(<?php echo URL('/'); ?>/admin/img/avatar1.jpg)"></div>
                            <div class="item-name">
                                John Ma<span>yer</span>s
                            </div>
                        </a>
                        <a class="ssg-item" href="users_profile_big.html">
                            <div class="item-media" style="background-image: url(<?php echo URL('/'); ?>/admin/img/avatar2.jpg)"></div>
                            <div class="item-name">
                                Th<span>omas</span> Mullier
                            </div>
                        </a>
                        <a class="ssg-item" href="users_profile_big.html">
                            <div class="item-media" style="background-image: url(<?php echo URL('/'); ?>/admin/img/avatar3.jpg)"></div>
                            <div class="item-name">
                                Kim C<span>olli</span>ns
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="search-suggestions-group">
                <div class="ssg-header">
                    <div class="ssg-icon">
                        <div class="os-icon os-icon-folder"></div>
                    </div>
                    <div class="ssg-name">
                        Files
                    </div>
                    <div class="ssg-info">
                        17 Total
                    </div>
                </div>
                <div class="ssg-content">
                    <div class="ssg-items ssg-items-blocks">
                        <a class="ssg-item" href="javascript:void(0);">
                            <div class="item-icon">
                                <i class="os-icon os-icon-file-text"></i>
                            </div>
                            <div class="item-name">
                                Work<span>Not</span>e.txt
                            </div>
                        </a>
                        <a class="ssg-item" href="javascript:void(0);">
                            <div class="item-icon">
                                <i class="os-icon os-icon-film"></i>
                            </div>
                            <div class="item-name">
                                V<span>ideo</span>.avi
                            </div>
                        </a>
                        <a class="ssg-item" href="javascript:void(0);">
                            <div class="item-icon">
                                <i class="os-icon os-icon-database"></i>
                            </div>
                            <div class="item-name">
                                User<span>Tabl</span>e.sql
                            </div>
                        </a>
                        <a class="ssg-item" href="javascript:void(0);">
                            <div class="item-icon">
                                <i class="os-icon os-icon-image"></i>
                            </div>
                            <div class="item-name">
                                wed<span>din</span>g.jpg
                            </div>
                        </a>
                    </div>
                    <div class="ssg-nothing-found">
                        <div class="icon-w">
                            <i class="os-icon os-icon-eye-off"></i>
                        </div>
                        <span>No files were found. Try changing your query...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layout-w">
        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
            <div class="mm-logo-buttons-w">
                <a class="mm-logo" href="{{route('welcome')}}"><img src="<?php echo URL('/'); ?>/admin/img/logo.png"><span>Clean Admin</span></a>
                <div class="mm-buttons">
                    <!-- <div class="content-panel-open">
                        <div class="os-icon os-icon-grid-circles"></div>
                    </div> -->
                    <div class="mobile-menu-trigger">
                        <div class="os-icon os-icon-hamburger-menu-1"></div>
                    </div>
                </div>
            </div>
            <div class="menu-and-user">
                <div class="logged-user-w">
                    <div class="avatar-w">
                        <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar1.jpg">
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">
                            Maria Gomez
                        </div>
                        <div class="logged-user-role">
                            Administrator
                        </div>
                    </div>
                </div>
                <ul class="main-menu">
                    <li class="has-sub-menu">
                        <a href="{{route('welcome')}}">
                            <div class="icon-w">
                                <div class="os-icon os-icon-layout"></div>
                            </div>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-users"></div>
                            </div>
                            <span>Asset Type</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('asset_type_list')}}">Asset Type</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
            <div class="logo-w">
                <a class="logo" href="{{route('welcome')}}">
                    <div style="display: inline;"><img alt="" src="<?php echo URL('/'); ?>/admin/img/logo/Mining_sales.jpg"></div>
                    <div class="logo-label">
                        CLM Report
                    </div>
                </a>
            </div>
            <div class="logged-user-w avatar-inline">
                <div class="logged-user-i">
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">
                            {{Auth::user()->name}}
                        </div>

                    </div>
                    <div class="logged-user-toggler-arrow">
                        <div class="os-icon os-icon-chevron-down"></div>
                    </div>
                    <div class="logged-user-menu color-style-bright">
                        <div class="logged-user-avatar-info">

                        <div class="logged-user-info-w">
                            <div class="logged-user-name" style="margin-left: -66%;">
                                {{Auth::user()->name}}
                            </div>

                        </div>
                    </div>
                    <div class="bg-icon">
                        <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
                    <ul>
                        <li>
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="os-icon os-icon-signs-11"></i><span>Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="menu-actions">
            <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
                <i class="os-icon os-icon-mail-14"></i>
                <div class="new-messages-count">
                    12
                </div>
                <div class="os-dropdown light message-list">
                    <ul>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar1.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    John Mayers
                                    </h6>
                                    <h6 class="message-title">
                                    Account Update
                                    </h6>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar2.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    Phil Jones
                                    </h6>
                                    <h6 class="message-title">
                                    Secutiry Updates
                                    </h6>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar3.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    Bekky Simpson
                                    </h6>
                                    <h6 class="message-title">
                                    Vacation Rentals
                                    </h6>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar4.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    Alice Priskon
                                    </h6>
                                    <h6 class="message-title">
                                    Payment Confirmation
                                    </h6>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-right">

                <div class="os-dropdown">
                    <ul>
                        <li>
                            <a href="users_profile_small.html"><i class="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
                        </li>
                        <li>
                            <a href="users_profile_small.html"><i class="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
                        </li>
                        <li>
                            <a href="users_profile_small.html"><i class="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
                        </li>
                        <li>
                            <a href="users_profile_small.html"><i class="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
                <i class="os-icon os-icon-zap"></i>
                <div class="new-messages-count">
                    4
                </div>
                <div class="os-dropdown light message-list">
                    <div class="icon-w">
                    <i class="os-icon os-icon-zap"></i>
                    </div>
                    <ul>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar1.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    John Mayers
                                    </h6>
                                    <h6 class="message-title">
                                    Account Update
                                    </h6>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar2.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    Phil Jones
                                    </h6>
                                    <h6 class="message-title">
                                    Secutiry Updates
                                    </h6>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar3.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    Bekky Simpson
                                    </h6>
                                    <h6 class="message-title">
                                    Vacation Rentals
                                    </h6>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="user-avatar-w">
                                    <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar4.jpg">
                                </div>
                                <div class="message-content">
                                    <h6 class="message-from">
                                    Alice Priskon
                                    </h6>
                                    <h6 class="message-title">
                                    Payment Confirmation
                                    </h6>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="element-search autosuggest-search-activator">
            <input placeholder="Start typing to search..." type="text">
        </div> -->
        <h1 class="menu-page-header">
            Page Header
        </h1>
        <ul class="main-menu">
            <li class="sub-header">
                <span>Actions</span>
            </li>
            <li class="selected has-sub-menu">
                <a href="{{route('welcome')}}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-layers"></div>
                    </div>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class=" has-sub-menu">
                <a href="javascript:void(0);">
                    <div class="icon-w">
                        <div class="os-icon os-icon-user"></div>
                    </div>
                    <span>Asset Types</span>
                </a>
                <div class="sub-menu-w">
                    <div class="sub-menu-header">
                    Users
                    </div>
                    <div class="sub-menu-icon">
                        <i class="os-icon os-icon-user"></i>
                    </div>
                    <div class="sub-menu-i">
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('asset_type_list')}}">Asset Type</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>

            <li class=" has-sub-menu">
                    <a href="javascript:void(0);">
                        <div class="icon-w">
                            <div class="os-icon os-icon-user"></div>
                        </div>
                        <span>Site</span>
                    </a>
                    <div class="sub-menu-w">
                        <div class="sub-menu-header">
                        Users
                        </div>
                        <div class="sub-menu-icon">
                            <i class="os-icon os-icon-user"></i>
                        </div>
                        <div class="sub-menu-i">
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{route('site_list')}}">Site</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user"></div>
                            </div>
                            <span>Asset</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">
                            Users
                            </div>
                            <div class="sub-menu-icon">
                                <i class="os-icon os-icon-user"></i>
                            </div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('asset_list')}}">Asset</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
            
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user"></div>
                            </div>
                            <span>Inspection</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">
                            Users
                            </div>
                            <div class="sub-menu-icon">
                                <i class="os-icon os-icon-user"></i>
                            </div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('inspection_list')}}">Inpection</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user"></div>
                            </div>
                            <span>Asset Inspection</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">
                            Users
                            </div>
                            <div class="sub-menu-icon">
                                <i class="os-icon os-icon-user"></i>
                            </div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('asset_inspection_list')}}">Asset Inpection</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user"></div>
                            </div>
                            <span>Asset Inspection Report</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">
                            Users
                            </div>
                            <div class="sub-menu-icon">
                                <i class="os-icon os-icon-user"></i>
                            </div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('asset_inspection_report_add')}}">Asset Inpection Report</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user"></div>
                            </div>
                            <span>Report</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">
                            Users
                            </div>
                            <div class="sub-menu-icon">
                                <i class="os-icon os-icon-user"></i>
                            </div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('report_view')}}">Report</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                
        </ul>

    </div>
<div class="content-w">
    <div class="top-bar color-scheme-transparent">
        <div class="top-menu-controls">
            {{-- <div class="element-search autosuggest-search-activator">
                <input placeholder="Start typing to search..." type="text">
            </div> --}}
            <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
                    <div class="os-dropdown light message-list">
                        <ul>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="user-avatar-w">
                                        <img alt="" src="img/avatar1.jpg">
                                    </div>
                                    <div class="message-content">
                                        <h6 class="message-from">
                                            John Mayers
                                        </h6>
                                        <h6 class="message-title">
                                            Account Update
                                        </h6>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="user-avatar-w">
                                        <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar2.jpg">
                                    </div>
                                    <div class="message-content">
                                        <h6 class="message-from">
                                            Phil Jones
                                        </h6>
                                        <h6 class="message-title">
                                            Secutiry Updates
                                        </h6>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="user-avatar-w">
                                        <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar3.jpg">
                                    </div>
                                    <div class="message-content">
                                        <h6 class="message-from">
                                            Bekky Simpson
                                        </h6>
                                        <h6 class="message-title">
                                            Vacation Rentals
                                        </h6>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="user-avatar-w">
                                        <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar4.jpg">
                                    </div>
                                    <div class="message-content">
                                        <h6 class="message-from">
                                            Alice Priskon
                                        </h6>
                                        <h6 class="message-title">
                                            Payment Confirmation
                                        </h6>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-left">
                    <div class="os-dropdown">
                        <div class="icon-w">
                            <i class="os-icon os-icon-ui-46"></i>
                        </div>

                    </div>
                </div>
                <div class="logged-user-w">
                    <div class="logged-user-i">
                        <div class="avatar-w">
                            <img alt="" src="<?php echo URL('/'); ?>/admin/img/avatar1.jpg">
                        </div>
                        <div class="logged-user-menu color-style-bright">
                            <div class="logged-user-avatar-info">
                                <div class="avatar-w">

                                </div>
                                <div class="logged-user-info-w">
                                    <div class="logged-user-name">
                                    {{Auth::user()->name}}
                                    </div>

                                </div>
                            </div>
                            <div class="bg-icon">
                                <i class="os-icon os-icon-wallet-loaded"></i>
                            </div>
                            <ul>
                                <li>
                                    <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <i class="os-icon os-icon-signs-11"></i><span>Logout</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield("content")
        <div class="floated-colors-btn third-floated-btn">
            <div class="os-toggler-w">
                <div class="os-toggler-i">
                    <div class="os-toggler-pill"></div>
                </div>
            </div>
            <span>Dark </span><span>Colors</span>
        </div>
    </div>
</div>
<div class="display-type"></div>
</div>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/moment/moment.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php echo URL('/'); ?>/admin/js/demo_customizer.js?version=4.5.0"></script>
    <script src="<?php echo URL('/'); ?>/admin/js/main.js?version=4.5.0"></script>
    <script src="<?php echo URL('/'); ?>/admin/js/dataTables.bootstrap4.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-XXXXXXXX-9', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>

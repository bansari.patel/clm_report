<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AssetInspectionReport extends Authenticatable
{
    protected $table = "asset_inspections_reports";
    protected $fillable = [
        'asset_id',
        'inspection_id',
        'next_due_date',
        'last_due_date',

    ];

    public function asset()
    {
        return $this->hasOne('App\Models\Asset','id','asset_id');
    }

    public function inspection()
    {
        return $this->hasOne('App\Models\Inspection','id','inspection_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AssetInspection extends Authenticatable
{
    protected $fillable = [
        'asset_id',
        'inspection_id',
        'last_due_date'
         
    ];

    public function asset()
    {
        return $this->hasOne('App\Models\Asset','id','asset_id');
    }

    public function inspection()
    {
        return $this->hasOne('App\Models\Inspection','id','inspection_id');
    }
}

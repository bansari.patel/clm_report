<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Asset extends Authenticatable
{
    protected $fillable = [
        'id','asset_type_id','site_id','name','status'

    ];

    public function asset_type()
    {
        return $this->hasOne('App\Models\AssetType','id','asset_type_id');
    }


    public function site()
    {
        return $this->hasOne('App\Models\Site','id','site_id');
    }


}

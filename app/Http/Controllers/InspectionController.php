<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Inspection;
class InspectionController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $list = Inspection::get();
        $data = [];
        $data['inspections'] = $list;
        return view('inspection.list',$data);
    }
    public function add()
    {
        return view('inspection.add');
    }
    public function save_Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'frequency' => 'required',
            'value' => 'required',
            'rate' => 'required'
            
        ]);
        if ($validator->fails()) {
            return redirect(route('inspection_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new Inspection();
        $add->name = $data['name'];
        $add->frequency = $data['frequency'];
        $add->value = $data['value'];
        $add->rate = $data['rate'];
        $add->save();
        
        return redirect('inspection/list');
    }
    public function edit($id)
    {
        $edit = Inspection::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('inspection.edit',$data);
    }
    public function save_edit(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
            'frequency' => 'required',
            'value' => 'required',
            'rate' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('inspection_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = Inspection::find($data['id']);
        $edit->name = $data['name'];
        $edit->status = $data['status'];
        $edit->frequency = $data['frequency'];
        $edit->value = $data['value'];
        $edit->rate = $data['rate'];
       
        $edit->save();
        
        return redirect('inspection/list');
    }
    public function delete($id)
    {
        $user = Auth::user();
        $delete= Inspection::find($id);
        $delete->delete();
        return redirect('inspection/list');
    }
}

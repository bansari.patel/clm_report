<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Inspection;
use App\Models\AssetInspection;
use App\Models\Asset;
use App\Models\AssetType;
use App\Models\AssetInspectionReport;
class AssetInspectionReportController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $list = AssetInspectionReport::with('asset','inspection')->get();
        $data = [];
        $data['lists'] = $list;
        return view('asset_inspection_report.list',$data);
    }

    public function add()
    {
        $asset = Asset::get();
        $inspection = Inspection::get();
        $data = [];
        $data['inspections'] = $inspection;
        $data['assets'] = $asset;

        return view('asset_inspection_report.add',$data);
    }
    public function save_Add(Request $request)
    {
        $data = $request->all();
        // print_r($data); exit;
        $validator = Validator::make($request->all(), [
            'due_date' => 'required',
            'date_inspected' => 'required',
            'asset_id' => 'required',
            'inspection_id' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('asset_inspection_report_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $add = new AssetInspectionReport();
        $add->next_due_date = $data['due_date'];
        $add->last_due_date = $data['date_inspected'];
        $add->asset_id = $data['asset_id'];
        $add->inspection_id = $data['inspection_id'];
        $add->save();
        
        return redirect('asset/inspection/report/list');
    }
    
    public function asset_type_list($id){
        $asset = Asset::find($id);
        $assetType = AssetType::where('id',$asset->asset_type_id)->get();
                    
        return json_encode($assetType);
    }

    public function inspection_list($id ,$aId){
        
        $data = AssetInspection::with('inspection')->where('inspection_id',$id)->where('asset_id',$aId)->get()->first();
                 
        return json_encode($data);
    }
}

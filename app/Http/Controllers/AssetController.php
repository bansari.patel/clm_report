<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Asset;
use App\Models\AssetType;
use App\Models\Site;

class AssetController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $list = Asset::get();
        $data = [];
        $data['assets'] = $list;
        return view('asset.list',$data);
    }
    public function add()
    {
        $asset_type = AssetType::get();
        $site = Site::get();

        $data = [];
        $data['asset_types'] = $asset_type;
        $data['sites'] = $site;
        
        
        return view('asset.add',$data);
    }
    public function save_Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'asset_type_id'=>'required',
            'site_id' =>'required'
            
        ]);
        if ($validator->fails()) {
            return redirect(route('asset_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new Asset();
        $add->name = $data['name'];
        $add->asset_type_id = $data['asset_type_id'];
        $add->site_id = $data['site_id'];
        $add->save();
        
        return redirect('asset/list');
    }
    public function edit($id)
    {
        $edit = Asset::find($id);

        $asset_type = AssetType::get();
        $site = Site::get();
        
        $data = [];
        $data['edit'] = $edit;
        $data['asset_types'] = $asset_type;
        $data['sites'] = $site;
       
        
        return view('asset.edit',$data);
    }
    public function save_edit(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'asset_type_id'=>'required',
            'site_id' =>'required',
            'status' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('asset_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = Asset::find($data['id']);
        $edit->name = $data['name'];
        $edit->asset_type_id = $data['asset_type_id'];
        $edit->site_id = $data['site_id'];
        $edit->status = $data['status'];
        $edit->save();
        
        return redirect('asset/list');
    }
    public function delete($id)
    {
        $user = Auth::user();
        $delete= Asset::find($id);
        $delete->delete();
        return redirect('asset/list');
    }
}

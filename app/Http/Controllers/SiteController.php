<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Site;
class SiteController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $list = Site::get();
        $data = [];
        $data['sites'] = $list;
        return view('site.list',$data);
    }
    public function add()
    {
        return view('site.add');
    }
    public function save_Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('site_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new Site();
        $add->name = $data['name'];
        $add->save();
        
        return redirect('site/list');
    }
    public function edit($id)
    {
        $edit = Site::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('site.edit',$data);
    }
    public function save_edit(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('site_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = Site::find($data['id']);
        $edit->name = $data['name'];
        $edit->status = $data['status'];
        $edit->save();
        
        return redirect('site/list');
    }
    public function delete($id)
    {
        $user = Auth::user();
        $delete= Site::find($id);
        $delete->delete();
        return redirect('site/list');
    }
}

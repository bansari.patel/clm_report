<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\AssetType;
class AssetTypeController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $list = AssetType::get();
        $data = [];
        $data['assetTypes'] = $list;
        return view('assetType.list',$data);
    }
    public function add()
    {
        return view('assetType.add');
    }
    public function save_Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('asset_type_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new AssetType();
        $add->name = $data['name'];
        $add->save();
        
        return redirect('asset/type/list');
    }
    public function edit($id)
    {
        $edit = AssetType::find($id);
        $data = [];
        $data['edit'] = $edit;
        return view('assetType.edit',$data);
    }
    public function save_edit(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('asset_type_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = AssetType::find($data['id']);
        $edit->name = $data['name'];
        $edit->status = $data['status'];
        $edit->save();
        
        return redirect('asset/type/list');
    }
    public function delete($id)
    {
        $user = Auth::user();
        $delete= AssetType::find($id);
        $delete->delete();
        return redirect('asset/type/list');
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Inspection;
use App\Models\AssetInspection;
use App\Models\Asset;
use App\Models\AssetType;
use App\Models\AssetInspectionReport;
use App\Models\Site;
class ReportController extends Controller
{
    public function index()
    {
        $asset = Asset::get();
        $assetType = AssetType::get();
        $sites = Site::get();

        $data = [];
        $data['assets'] = $asset;
        $data['asset_types'] = $assetType;
        $data['sites'] = $sites;
        return view('report.view',$data);
    }

    public function list()
    {
        $lists = AssetInspectionReport::with('asset','inspection')->get();
        $data = [];
        $data['lists'] = $lists;
        
        return view('report.list',$data);
    }
    public function filter_list(Request $request)
    {
        $dataPost = $request->all();
        if(isset($dataPost['asset_type_btn'])){
            $validator = Validator::make($request->all(),[
                'asset_type_id'=>'required',
            ]);
        }
        if(isset($dataPost['asset_btn'])){
            $validator = Validator::make($request->all(),[
                'asset_id'=>'required',
            ]);
        }
        if(isset($dataPost['site_btn'])){
            $validator = Validator::make($request->all(),[
                'site_id'=>'required',
            ]);
        }
        if(isset($dataPost['forecast_btn'])){
            $validator = Validator::make($request->all(),[
                'date_id'=>'required',
            ]);
        }


        if($validator->fails()){
            return redirect(route('report_view'))
            ->withErrors($validator)
            ->withInput();
        }


        if(isset($dataPost['asset_type_btn'])) {
            $assetType =AssetType::where('id',$dataPost['asset_type_id'])->get()->first();
            $asset= Asset::where('asset_type_id',$assetType->id)->get();
            $assetId = [];
            foreach($asset as $ass){
                array_push($assetId,$ass->id);
            }
            $lists = AssetInspectionReport::with('asset','inspection')->whereIn('asset_id',$assetId)->get();
        
        }else if(isset($dataPost['asset_btn'])){
            $lists = AssetInspectionReport::with('asset','inspection')->where('asset_id',$dataPost['asset_id'])->get();
        }else if(isset($dataPost['site_btn'])){
            $asset= Asset::where('site_id',$dataPost['site_id'])->get();
            $assetId = [];
            foreach($asset as $ass){
                array_push($assetId,$ass->id);
            }

            $lists = AssetInspectionReport::with('asset','inspection')->whereIn('asset_id',$assetId)->get();
        }
        else{
            if($dataPost['date_id'] == 0){
                
                $todayDate = date("yy-m-d");
                $month = date("m");
                $date = date("d");
                $lists = AssetInspectionReport::with('asset','inspection')->where('created_at', '>=', date('Y-m-d').' 00:00:00')->whereMonth('created_at', $month)->get();
                
            }
            else{
                $lists = AssetInspectionReport::with('asset','inspection')->whereMonth('created_at', $dataPost['date_id'])->get();
            }
        }
        
        $data = [];
        $data['lists'] = $lists;
        
        return view('report.list',$data);
    }

    
    
}

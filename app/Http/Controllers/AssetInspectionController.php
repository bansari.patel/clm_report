<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Inspection;
use App\Models\AssetInspection;
use App\Models\Asset;
class AssetInspectionController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $list = AssetInspection::with('asset','inspection')->get();
        $data = [];
        $data['assetInspections'] = $list;
        return view('asset_inspection.list',$data);
    }
    public function add()
    {
        $asset = Asset::get();
        $inspeaction = Inspection::get();

        $data = [];
        $data['inspections'] = $inspeaction;
        $data['assets'] = $asset;

        return view('asset_inspection.add',$data);
    }
    public function save_Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'last_due_date' => 'required',
            'asset_id' => 'required',
            'inspection_id' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('asset_inspection_add'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $add = new AssetInspection();
        $add->last_due_date = $data['last_due_date'];
        $add->asset_id = $data['asset_id'];
        $add->inspection_id = $data['inspection_id'];
        $add->save();
        
        return redirect('asset/inspection/list');
    }
    public function edit($id)
    {
        $edit = AssetInspection::find($id);

        $asset = Asset::get();
        $inspeaction = Inspection::get();

        $data = [];
        $data['inspections'] = $inspeaction;
        $data['assets'] = $asset;
        $data['edit'] = $edit;
        return view('asset_inspection.edit',$data);
    }
    public function save_edit(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'last_due_date' => 'required',
            'asset_id' => 'required',
            'inspection_id' => 'required',
            
        ]);
        if ($validator->fails()) {
            return redirect(route('inspection_edit_view',$data['id']))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $edit = AssetInspection::find($data['id']);
        $edit->last_due_date = $data['last_due_date'];
        $edit->asset_id = $data['asset_id'];
        $edit->inspection_id = $data['inspection_id'];
        $edit->save();
        
        return redirect('asset/inspection/list');
    }
    public function delete($id)
    {
        $user = Auth::user();
        $delete= AssetInspection::find($id);
        $delete->delete();
        return redirect('asset/inspection/list');
    }
}
